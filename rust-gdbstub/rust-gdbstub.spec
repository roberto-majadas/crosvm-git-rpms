# Generated by rust2rpm 22
%global debug_package %{nil}

%global crate gdbstub

Name:           rust-gdbstub
Version:        0.6.3
Release:        %autorelease
Summary:        Implementation of the GDB Remote Serial Protocol in Rust

License:        MIT OR Apache-2.0
URL:            https://crates.io/crates/gdbstub
Source:         %{crates_source}

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust-packaging >= 21

%global _description %{expand:
Implementation of the GDB Remote Serial Protocol in Rust.}

%description %{_description}

%package        devel
Summary:        %{summary}
BuildArch:      noarch

%description    devel %{_description}

This package contains library source intended for building other packages which
use the "%{crate}" crate.

%files          devel
%license %{crate_instdir}/LICENSE
%license %{crate_instdir}/docs/LICENSE-APACHE
%license %{crate_instdir}/docs/LICENSE-MIT
%doc %{crate_instdir}/CHANGELOG.md
%doc %{crate_instdir}/README.md
%{crate_instdir}/

%package     -n %{name}+default-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+default-devel %{_description}

This package contains library source intended for building other packages which
use the "default" feature of the "%{crate}" crate.

%files       -n %{name}+default-devel
%ghost %{crate_instdir}/Cargo.toml

%package     -n %{name}+__dead_code_marker-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+__dead_code_marker-devel %{_description}

This package contains library source intended for building other packages which
use the "__dead_code_marker" feature of the "%{crate}" crate.

%files       -n %{name}+__dead_code_marker-devel
%ghost %{crate_instdir}/Cargo.toml

%package     -n %{name}+alloc-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+alloc-devel %{_description}

This package contains library source intended for building other packages which
use the "alloc" feature of the "%{crate}" crate.

%files       -n %{name}+alloc-devel
%ghost %{crate_instdir}/Cargo.toml

%package     -n %{name}+paranoid_unsafe-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+paranoid_unsafe-devel %{_description}

This package contains library source intended for building other packages which
use the "paranoid_unsafe" feature of the "%{crate}" crate.

%files       -n %{name}+paranoid_unsafe-devel
%ghost %{crate_instdir}/Cargo.toml

%package     -n %{name}+std-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+std-devel %{_description}

This package contains library source intended for building other packages which
use the "std" feature of the "%{crate}" crate.

%files       -n %{name}+std-devel
%ghost %{crate_instdir}/Cargo.toml

%package     -n %{name}+trace-pkt-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+trace-pkt-devel %{_description}

This package contains library source intended for building other packages which
use the "trace-pkt" feature of the "%{crate}" crate.

%files       -n %{name}+trace-pkt-devel
%ghost %{crate_instdir}/Cargo.toml

%prep
%autosetup -n %{crate}-%{version_no_tilde} -p1
%cargo_prep

%generate_buildrequires
%cargo_generate_buildrequires

%build
%cargo_build

%install
%cargo_install

%changelog
%autochangelog
