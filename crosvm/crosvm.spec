%bcond_without check

%global     gitcommit	   2824a8e1636bcf638b24cf7c846f3f76768e4492
%global     gitshortcommit %(c=%{gitcommit}; echo ${c:0:7})
%global     gitsnapinfo    .20221110git%{gitshortcommit}
%global     minijail_gitcommit	     77c59db8ff9ab7795ca1e986e2658b02d9252498
%global     libtpm2_gitcommit        bc3bb265b7315a187ea5e5067f7e50c4469416c7
%global     minigbm_gitcommit        ef852e007ce7663e1f8c81dedc1ec5efc7c81e30
%global     virglrenderer_gitcommit  4770f706db2d0e7731264cb36a659c764c7787f1

Name:           crosvm
Version:        0.1.0
Release:        1%{?gitsnapinfo}%{?dist}
Summary:        # FIXME

License:        MIT
URL:            https://chromium.googlesource.com/crosvm/crosvm
Source0:        https://chromium.googlesource.com/crosvm/crosvm/+archive/%{gitcommit}.tar.gz
Source1:        https://chromium.googlesource.com/chromiumos/platform/minijail/+archive/%{minijail_gitcommit}.tar.gz
Source2:        https://chromium.googlesource.com/chromiumos/third_party/tpm2/+archive/%{libtpm2_gitcommit}.tar.gz
Source3:        https://chromium.googlesource.com/chromiumos/platform/minigbm/+archive/%{minigbm_gitcommit}.tar.gz
Source4:        https://chromium.googlesource.com/chromiumos/third_party/virglrenderer/+archive/%{virglrenderer_gitcommit}.tar.gz
Patch0:         crosvm-remove-cargo-win-deps.diff
Patch1:         crosvm-fix-cargo-confg.diff

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust-packaging >= 21
BuildRequires:  (crate(anyhow/default) >= 1.0.32 with crate(anyhow/default) < 2.0.0~)
BuildRequires:  (crate(argh/default) >= 0.1.0 with crate(argh/default) < 0.2.0~)
BuildRequires:  (crate(async-task/default) >= 4.0.0 with crate(async-task/default) < 5.0.0~)
BuildRequires:  (crate(async-trait/default) >= 0.1.36 with crate(async-trait/default) < 0.2.0~)
BuildRequires:  (crate(cfg-if/default) >= 1.0.0 with crate(cfg-if/default) < 2.0.0~)
BuildRequires:  (crate(chrono/default) >= 0.4.0 with crate(chrono/default) < 0.5.0~)
BuildRequires:  (crate(document-features/default) >= 0.2.6 with crate(document-features/default) < 0.3.0~)
BuildRequires:  (crate(enumn/default) >= 0.1.0 with crate(enumn/default) < 0.2.0~)
BuildRequires:  (crate(env_logger/default) >= 0.9.0 with crate(env_logger/default) < 0.10.0~)
BuildRequires:  (crate(futures/default) >= 0.3.0 with crate(futures/default) < 0.4.0~)
BuildRequires:  (crate(gdbstub_arch/default) >= 0.2.2 with crate(gdbstub_arch/default) < 0.3.0~)
BuildRequires:  (crate(gdbstub/default) >= 0.6.1 with crate(gdbstub/default) < 0.7.0~)
BuildRequires:  (crate(libc/default) >= 0.2.93 with crate(libc/default) < 0.3.0~)
BuildRequires:  (crate(log/default) >= 0.0.0 with crate(log/default) < 1.0.0~)
BuildRequires:  (crate(log/release_max_level_debug) >= 0.0.0 with crate(log/release_max_level_debug) < 1.0.0~)
BuildRequires:  (crate(nom/default) >= 7.1.1 with crate(nom/default) < 8.0.0~)
BuildRequires:  (crate(num-traits/default) >= 0.2.0 with crate(num-traits/default) < 0.3.0~)
BuildRequires:  (crate(memoffset/default) >= 0.6.0 with crate(memoffset/default) < 0.7.0~)
BuildRequires:  (crate(once_cell/default) >= 1.7.0 with crate(once_cell/default) < 2.0.0~)
BuildRequires:  (crate(protobuf/default) >= 2.3.0 with crate(protobuf/default) < 3.0.0~)
BuildRequires:  (crate(rand/default) >= 0.8.0 with crate(rand/default) < 0.9.0~)
BuildRequires:  (crate(remain/default) >= 0.2.0 with crate(remain/default) < 0.3.0~)
BuildRequires:  (crate(scudo/default) >= 0.1.0 with crate(scudo/default) < 0.2.0~)
BuildRequires:  (crate(serde/default) >= 1.0.0 with crate(serde/default) < 2.0.0~)
BuildRequires:  (crate(serde/derive) >= 1.0.0 with crate(serde/derive) < 2.0.0~)
BuildRequires:  crate(serde_json/default) >= 0.0.0
BuildRequires:  (crate(smallvec/default) >= 1.6.1 with crate(smallvec/default) < 2.0.0~)
BuildRequires:  (crate(static_assertions/default) >= 1.1.0 with crate(static_assertions/default) < 2.0.0~)
BuildRequires:  (crate(tempfile/default) >= 3.0.0 with crate(tempfile/default) < 4.0.0~)
BuildRequires:  (crate(terminal_size/default) >= 0.1.17 with crate(terminal_size/default) < 0.2.0~)
BuildRequires:  (crate(thiserror/default) >= 1.0.20 with crate(thiserror/default) < 2.0.0~)
BuildRequires:  (crate(uuid/default) >= 0.8.2 with crate(uuid/default) < 0.9.0~)
BuildRequires:  (crate(uuid/serde) >= 0.8.2 with crate(uuid/serde) < 0.9.0~)
BuildRequires:  (crate(uuid/v4) >= 0.8.2 with crate(uuid/v4) < 0.9.0~)
BuildRequires:  (crate(downcast-rs/default) >= 1.2.0 with crate(downcast-rs/default) < 2.0.0~)
BuildRequires:  (crate(fnv/default) >= 1.0.0 with crate(fnv/default) < 2.0.0~)
BuildRequires:  (crate(protobuf/default) >= 2.24.0 with crate(protobuf/default) < 3.0.0~)
BuildRequires:  (crate(protobuf/with-serde) >= 2.24.0 with crate(protobuf/with-serde) < 3.0.0~)
BuildRequires:  (crate(protoc-rust/default) >= 2.24.0 with crate(protoc-rust/default) < 3.0.0~)
BuildRequires:  crate(pkg-config/default) >= 0.0.0
BuildRequires:  (crate(intrusive-collections/default) >= 0.9.0 with crate(intrusive-collections/default) < 0.10.0~)
BuildRequires:  (crate(crossbeam-utils/default) >= 0.8.0 with crate(crossbeam-utils/default) < 0.9.0~)
BuildRequires:  (crate(crc32fast/default) >= 1.2.1 with crate(crc32fast/default) < 2.0.0)
BuildRequires:  (crate(dbus/default) >= 0.9.0 with crate(dbus/default) < 0.10.0~)
BuildRequires:  (crate(libudev/default) >= 0.2.0 with crate(libudev/default) < 0.3.0~)
BuildRequires:  (crate(bytes/default) >= 1.1.0 with crate(bytes/default) < 2.0.0~)

BuildRequires: libcap-devel
BuildRequires: wayland-devel
BuildRequires: wayland-protocols-devel
BuildRequires: libepoxy-devel
BuildRequires: libdrm-devel
BuildRequires: meson
BuildRequires: pkg-config

%description

%files
%license LICENSE
%doc ARCHITECTURE.md
%doc CONTRIBUTING.md
%doc README.chromeos.md
%doc README.md
%{_bindir}/crosvm
%{_datadir}/policy/crosvm

%prep
%autosetup -n %{name}-%{gitshortcommit} -p1 -c

gzip -dc %{SOURCE1} | tar -C third_party/minijail -xvvf  -

gzip -dc %{SOURCE2} | tar -C tpm2-sys/libtpm2 -xvvf  -
mkdir tpm2-sys/libtpm2/.git

gzip -dc %{SOURCE3} | tar -C third_party/minigbm -xvvf  -
mkdir third_party/minigbm/.git

gzip -dc %{SOURCE4} | tar -C third_party/virglrenderer -xvvf  -
mkdir third_party/virglrenderer/.git

%cargo_prep
cat .cargo/config.toml >> .cargo/config
rm .cargo/config.toml

%build
%cargo_build -f "gpu,virgl_renderer,virgl_renderer_next"

%install
%cargo_install 

install -d -m0755 %buildroot%_datadir/policy/crosvm

%ifarch x86_64
install -Dp -m0644 seccomp/x86_64/*.policy -t %buildroot%_datadir/policy/crosvm
%endif
%ifarch aarch64
install -Dp -m0644 seccomp/aarch64/*.policy -t %buildroot%_datadir/policy/crosvm
%endif

%changelog
* Thu Nov 10 2022 Roberto Majadas <rmajadas@redhat.com>
- Update crossvm to git commit 2824a8e1636bcf638b24cf7c846f3f76768e4492
* Tue Oct 04 2022 Roberto Majadas <rmajadas@redhat.com>
- 
